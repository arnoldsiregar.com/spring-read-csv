package com.arnoldsiregar.spring.readcsv;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ComplexReader {

    public void read() throws IOException {
        List<List<String>> records = new ArrayList<List<String>>();
        try (CSVReader csvReader = new CSVReader(new FileReader("csv/complex.csv"))) {
            String[] values = null;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));

                System.out.println("Record: " + Arrays.asList(values));
            }
        }

        System.out.println("Records:" + records);
    }
}
