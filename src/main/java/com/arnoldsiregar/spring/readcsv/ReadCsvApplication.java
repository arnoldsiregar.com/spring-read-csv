package com.arnoldsiregar.spring.readcsv;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadCsvApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ReadCsvApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
        System.out.println("SimpleReader1:");
	    SimpleReader1 reader1 = new SimpleReader1();
		reader1.read();

        System.out.println("SimpleReader2:");
        SimpleReader2 reader2 = new SimpleReader2();
        reader2.read();

        System.out.println("ComplexReader:");
        ComplexReader reader3 = new ComplexReader();
        reader3.read();
	}
}
