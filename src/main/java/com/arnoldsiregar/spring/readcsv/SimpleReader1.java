package com.arnoldsiregar.spring.readcsv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimpleReader1 {
    private static final String COMMA_DELIMITER = ",";

    public void read() throws IOException {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("csv/simple.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));

                System.out.println("Record: " + Arrays.asList(values));
            }
        }

        System.out.println("Records:" + records);
    }
}
